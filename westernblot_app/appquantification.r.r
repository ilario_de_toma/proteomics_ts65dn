setwd("~/OneDrive - CRG - Centre de Regulacio Genomica/Dropbox (CRG)/proteomic_trisomic/paper_trisomic/")


datiapp=read.csv("appquant.csv", row.names = 1)

datiapp$group=c(rep("wt",5),rep("ts",5))
require(ggplot2)


p <- ggplot(datiapp, aes(x=group, y=ratio)) + geom_boxplot(outlier.size=1.5, fill=c("black", "red"), color=c(rep("black", 5), rep("red",5)))+
  geom_jitter(shape=16, position=position_jitter(0.2))+ylab("app/actb (intensity)")  + theme_bw()
# Box plot with jittered points
# 0.2 : degree of jitter in x direction
p