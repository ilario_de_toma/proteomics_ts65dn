## Proteomics Rmd file

This repository contains the Rmd code for reproducing the analysis of the proteome and phosphoproteome of Ts65Dn mice, untreated or treated with green tea, Environmental Enrichment or a combination of both.

Once the user manually install all the packages loaded in the R mark down file, it will be possible to reproduce the analysis by setting the working directly in the same folder where all the input file of this resitory are loaded.

These input file are produced with MSstats as explained in our manuscript.